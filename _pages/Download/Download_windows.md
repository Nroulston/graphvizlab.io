---
layout: page
title: Windows Packages
redirect_from:
  # We want to redirect from /Download_windows.php. We need ".php.html" else the
  # redirect page is downloaded to ~/Downloads/ rather than shown in browser. See:
  # https://github.com/jekyll/jekyll-redirect-from/issues/145#issuecomment-392277818
  - /Download_windows.php.html
---
	
**Note**: These Visual Studio packages do not alter the PATH variable or access the registry at all. 
If you wish to use the command-line interface to Graphviz or are using some other program that calls
 a Graphviz program, you will need to set the PATH variable yourself.

## 2.38 Stable Release

* [graphviz-2.38.msi](windows/graphviz-2.38.msi)
* [graphviz-2.38.zip](windows/graphviz-2.38.zip)

